use actix_web::{get, web, App, HttpResponse, HttpServer, Responder};

fn index() -> impl Responder {
    HttpResponse::Ok().body("Hello Actix-Web")
}

#[get("/hello")]
fn index3() -> impl Responder {
    HttpResponse::Ok().body("Hey there again!")
}

fn main() {
    HttpServer::new(|| App::new().route("/", web::get().to(index)).service(index3))
        .bind("127.0.0.1:8080")
        .unwrap()
        .run()
        .unwrap();
}
